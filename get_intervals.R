get_intervals <- function(intervals_fn) {
  tryCatch({intervals <- read.table(intervals_fn,
                                    sep='\t',
                                    header = FALSE,
                                    stringsAsFactors = FALSE,
                                    colClasses = rep('numeric', 2))
            if (any(intervals[,2] <= intervals[,1])) stop('Interval ends must be greater than the starts')
            if (any(intervals[intervals>1 | intervals<0])) stop('Interval values must be between 0 and 1') 
            return(intervals) },
           error = function(e) {
             stop(sprintf('The intervals file is not well formatted\nError in %s: %s\n',
                          paste(deparse(e$call),collapse=''),
                          e$message))
           })
}
