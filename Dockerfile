# Dockerfile for shiny
#
# R 4.0.2 shiny-server app exposed in 4040
# 
# Izaskun Mallona
# 17 Aug 2020

# docker build -t methylation_plotter .
# docker run --rm -p 4141:3838 --name methylation_plotter methylation_plotter
# docker exec -it methylation_plotter /bin/bash

FROM rocker/shiny:4.0.0

# system libraries of general use
RUN apt-get update && apt-get install -y \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    git \
    libxml2 \
    libxml2-dev \
    r-cran-xml \
    libxt-dev \
    libcairo2-dev \
    libgtk2.0-dev \
    xvfb xauth xfonts-base
    
  

# install R packages required

# RUN R -e "install.packages('devtools', repos='http://cran.rstudio.com/')"

# RUN R -e "install.packages('BiocManager', repos='http://cran.rstudio.com/')"

RUN R -e "install.packages(c('Cairo', 'shiny', 'lattice', 'dendextend', 'gplots'))"


# code

RUN git clone https://bitbucket.org/imallona/methylation_plotter

RUN mv methylation_plotter /srv/shiny-server

RUN cd /srv/shiny-server/methylation_plotter

COPY www/index.html /srv/shiny-server/methylation_plotter/www/



RUN cd /srv/shiny-server/methylation_plotter/www

RUN ln -s /usr/local/lib/R/site-library/shiny/www/shared/bootstrap/
RUN ln -s /usr/local/lib/R/site-library/shiny/www/shared/shiny.css
RUN ln -s /usr/local/lib/R/site-library/shiny/www/shared/shiny.js
RUN ln -s /usr/local/lib/R/site-library/shiny/www/shared/shiny.min.js
RUN ln -s /usr/local/lib/R/site-library/shiny/www/shared/bootstrap/js/bootstrap.js
RUN ln -s /usr/local/lib/R/site-library/shiny/www/shared/bootstrap/js/bootstrap.min.js

COPY  server.R /srv/shiny-server/methylation_plotter/

COPY www/white.CairoPNG /srv/shiny-server/methylation_plotter/www/white.CairoPNG

COPY shiny-server.conf /etc/shiny-server

# select port
EXPOSE 3838

# # allow permission
RUN sudo chown -R shiny:shiny /srv/shiny-server
RUN sudo chown -R shiny:shiny /var/lib/shiny-server

# # run app
# CMD ["/usr/bin/shiny-server.sh"]

