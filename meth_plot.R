#!/usr/bin/env R
#
# @package meth_plotter
# @author Anna Diez
# @author Izaskun Mallona

methmax<-1
maxSamples<-100
maxCpG<-100


meth_plot <- function(dd, intervals, SampleOrder, plotType, position, grup){
  # recoverign the sample sorting start
  
  if(length(which(table(position)>1))>0) stop("Repeated positions are not allowed")
  if(length(which(dd<0))>0) stop("Negative values not allowed")
  if(length(which(dd>methmax))>0) stop(paste0("Values greater than ",methmax," are not allowed"))

  
  if(SampleOrder == "as-is"){
    dd <- dd[seq(dim(dd)[1],1),]
    grup <- grup[seq(dim(dd)[1],1)]
  }
  
  if(SampleOrder == "by-group"){
    dd <- dd[order(grup,decreasing=TRUE),]
    grup <- grup[order(grup,decreasing=TRUE)]
  }
  
  if(SampleOrder == "by-meth"){
    mdd <- apply(dd,1,mean,na.rm=TRUE)
    dd <- dd[order(mdd),]
    grup <- grup[order(mdd)]
  }
  
  if(SampleOrder == "clust"){
    hc <- hclust(dist(dd))
    dendro <- rev(as.dendrogram(hc))
    dd <- dd[order.dendrogram(dendro),]
    grup <- grup[order.dendrogram(dendro)]
  }
  
  # recoverign the sample sorting end
  
  #################################################################################################
  #################################################################################################
  if (plotType == "proportional" || plotType == "nonproportional"){
    
    if(dim(dd)[2]>maxCpG) stop(paste0("plot type circles only allows a maximum of ", maxCpG," positions."))
    if(dim(dd)[1]>maxSamples) stop(paste0("plot type circles only allows a maximum of ", maxSamples," samples."))   
    
    colores <- function(x){
      grisos <- rev(gray.colors((dim(intervals)[1]-2)))
      if(x>=intervals[1,1] & x<=intervals[1,2] & !is.na(x)) colorin <- "#FFFFFF"
      for(k in 2:(dim(intervals)[1]-1)){
        if(x>=intervals[k,1] & x<=intervals[k,2] & !is.na(x)) colorin <- grisos[k-1]
      }
      if(x>=intervals[dim(intervals)[1],1] & x<=intervals[dim(intervals)[1],2] | is.na(x)) colorin <- "#000000"
      return(colorin)
    }
    color_legend<-c("#FFFFFF",rev(gray.colors((dim(intervals)[1]-2))),"#000000")
    
    
    
    formas <- function(x){
      if(is.na(x)) forma <- 4
      if(!is.na(x)) forma <- 21
      return(forma)
    }
    
    #################################################################################################
    #################################################################################################
    
    if(plotType=="proportional"){
      xmax <- max(position)+(((max(position))-min(position))/2)
      xmin <- min(position)
      ymax <- dim(dd)[1]
      colplot <- colores(dd[1,1])
      formplot <- formas(dd[1,1])
      par(mai=par()$mai+c(0.5,1,0,0))
      plot(position[1], 1, pch=formplot, col="black", bg=colplot, cex=2, xlim=c(xmin,xmax), ylim=c(1,ymax), axes=FALSE, xlab="", ylab="")
      axis(1, at=position, labels=paste0(position), las=2, srt=45)
      axis(2, c(1:ymax), rownames(dd)[1:(dim(dd)[1])], las=1)
      abline(h=c(1:ymax), lty=1, col="dimgray")
      
      for(j in 1:dim(dd)[1]){
        for(i in 1:dim(dd)[2]){
          colplot <- colores(dd[j,i])
          formplot <- formas(dd[j,i])
          points(position[i], j, pch=formplot, col="black", bg=colplot, cex=2)
        }
      }
      legend("right", c(paste0(intervals[,1],"-",intervals[,2]),"Not available"), fill=NULL, border="white", bty="o", pt.cex=1.5, pch=c(rep(21,dim(intervals)[1]),4), pt.bg=c("#FFFFFF",rev(gray.colors((dim(intervals)[1]-2))),"#000000"),bg="white")
      box(lwd=2)
      par(mai=par()$mai+c(-0.5,-1,0,0))

    }
    
    #################################################################################################
    #################################################################################################
    
    if(plotType=="nonproportional"){
      xmax <- dim(dd)[2]+((dim(dd)[2]-1)/2)
      xmin <- 1
      ymax <- dim(dd)[1]
      
      colplot <- colores(dd[1,1])
      formplot <- formas(dd[1,1])
      par(mai=par()$mai+c(0.5,1,0,0))
      plot(1, 1, pch=formplot, col="black", bg=colplot, cex=2, xlim=c(xmin,xmax), ylim=c(1,ymax), axes=FALSE, xlab="", ylab="")
      axis(1, at=c(1:dim(dd)[2]), labels=paste0(position), las=2, srt=45)
      axis(2, at=c(1:ymax), rownames(dd)[1:(dim(dd)[1])], las=1)
      abline(h=c(1:ymax), lty=1, col="dimgray")
      
      for(j in 1:dim(dd)[1]){
        for(i in 1:dim(dd)[2]){
          colplot <- colores(dd[j,i])
          formplot <- formas(dd[j,i])
          points(i, j, pch=formplot, col="black", bg=colplot, cex=2)
        }
      }
      legend("right", c(paste0(intervals[,1],"-",intervals[,2]),"Not available"), fill=NULL, border="white", bty="o", pt.cex=1.5, pch=c(rep(21,dim(intervals)[1]),4), pt.bg=c("#FFFFFF",rev(gray.colors((dim(intervals)[1]-2))),"#000000"),bg="white")
      box(lwd=2)
      par(mai=par()$mai+c(-0.5,-1,0,0))
      
    }
  }
  
  
  
  #################################################################################################
  #################################################################################################
  if(plotType == "grid"){
    dd2 <- dd#[dim(dd)[1]:1,]
    dd2[which(is.na(dd2))] <- -0.1
    #     qwe <- as.vector(col2rgb("lightblue"))
    #     rgb(173,216,230, maxColorValue=255)
    library(lattice)
    #    library(latticeExtra)
    if(dim(dd2)[1]<=dim(intervals)[1]) legendheight <- 1
    if(dim(dd2)[1]>dim(intervals)[1]) legendheight <- dim(intervals)[1]/dim(dd2)[1]
    colkey <- c("#ADD8E6","#FFFFFF",rev(gray.colors((dim(intervals)[1]-2))),"#000000")
    
    
    
    cuts <- c(intervals[1,1],intervals[1:(dim(intervals)[1]-1),2])-0.000001
    cutfinal<-intervals[dim(intervals)[1],2]
    print(obj1 <- levelplot(t(dd2), at=c(-0.1,cuts,cutfinal), col.regions=colkey, xlab="", ylab="",
                            colorkey=list(height=legendheight, width=1, at=c(-0.1,c(cuts+0.000001,cutfinal)), 
                                          labels=c("NA",paste0(c(cuts+0.000001,cutfinal))),
                                          col=colkey),
                            scales=list(y=list(labels=paste(grup,rownames(dd2))), tck=c(1,0),
                                        x=list(labels=position, rot=90)), border="black"))
  }
  
}
