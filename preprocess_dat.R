#!/usr/bin/env R
#
# @package meth_plotter
# @author Izaskun Mallona
# @author Anna Diez

preprocess_dat <- function(dd) {
  
  dd <- dd[,order(as.numeric(dd[dim(dd)[1],]))]
  grup <- as.character(dd[1:(dim(dd)[1]-1), dim(dd)[2]])
  dd <- dd[,-dim(dd)[2]]
  position <- as.numeric(dd[dim(dd)[1],])
  dd <- as.matrix(dd[-dim(dd)[1],])
    
  to_return <- list()
  to_return[["dd"]] <- dd
  to_return[["grup"]] <- grup
  to_return[["position"]] <- position
  #to_return[['hclust']] <- hc
  
  return(to_return)
}
